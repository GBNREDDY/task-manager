import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo-item-detail',
  templateUrl: './todo-item-detail.component.html',
  styleUrls: ['./todo-item-detail.component.css']
})
export class TodoItemDetailComponent implements OnInit {

  pid: string = null;
  tid: string = null;
  iid: string = null;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.pid = this.route.snapshot.paramMap.get('pid');
    this.tid = this.route.snapshot.paramMap.get('tid');
    this.iid = this.route.snapshot.paramMap.get('iid');
  }

}
