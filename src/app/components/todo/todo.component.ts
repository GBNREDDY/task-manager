import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from 'src/app/services/todo.service';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  pid: string = null;
  todos: Todo[];
  constructor(private route: ActivatedRoute, private todoService: TodoService) { }

  ngOnInit() {
    this.pid = this.route.snapshot.paramMap.get('pid');
    this.todoService.getTodo(this.pid).subscribe(todos => {
      this.todos = todos;
    });
  }

  //add todo
  addTodo(pname: string) {
    const p = {
      name: pname,
      project_id: Number(this.pid)
    };
    this.todoService.addTodo(p).subscribe(todo => {
      this.todos.push(todo);
    })
  }

  //delete todo
  deleteTodo(id: number) {
    this.todoService.deleteTodo(id).subscribe(todo => {
      this.todos = this.todos.filter(item => { return item.id != id });
    })
  }

}
