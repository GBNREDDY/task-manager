import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { TodoItemService } from 'src/app/services/todo-item.service';
import { TodoItem } from 'src/app/models/todo-item';


@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  pid: string = null;
  tid: string = null;
  todoItems: TodoItem[];
  constructor(private route: ActivatedRoute, private todoItemServie: TodoItemService) { }

  ngOnInit() {
    this.pid = this.route.snapshot.paramMap.get('pid');
    this.tid = this.route.snapshot.paramMap.get('tid');

    this.todoItemServie.getTodoItem(this.pid, this.tid).subscribe(todoItems => {
      this.todoItems = todoItems;
    });
  }
  //add todo-item
  addTodoItem(pname: string) {
    const p = {
      name: pname,
      project_id: Number(this.pid),
      todo_id: Number(this.tid),
      completed: false
    };
    this.todoItemServie.addTodoItem(p).subscribe(todoItem => {
      this.todoItems.push(todoItem);
    })
  }

  //delete todo-item
  deleteTodoItem(id: number) {
    this.todoItemServie.deleteTodoItem(id).subscribe(todoitem => {
      this.todoItems = this.todoItems.filter(item => { return item.id != id });
    })
  }

  changeStatus(p: TodoItem, e: any) {
    p.completed = !p.completed;
    this.todoItemServie.updateTodoItem(p).subscribe(todoItem => {
    })
  }
}
