import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  projects: Project[];
  constructor(private projectService: ProjectService) {

  }
  ngOnInit() {
    this.projectService.getProject().subscribe(projects => {
      this.projects = projects;
    });
  }

  //add project
  addProject(pname: string) {
    const p = {
      name: pname
    };
    this.projectService.addProject(p).subscribe(project => {
      this.projects.push(project);
    })
  }

  //delete project
  deleteProject(id: number) {
    this.projectService.deleteProject(id).subscribe(project => {
      this.projects = this.projects.filter(item => { return item.id != id });
    })
  }

}
