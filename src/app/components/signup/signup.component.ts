import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupService } from 'src/app/services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email: string;
  password: string;
  name: string;
  constructor(private router: Router, private signupService: SignupService) {
    if (localStorage.getItem('login') == 'true') {
      this.router.navigateByUrl('/project');
    }
  }

  ngOnInit() {
  }

  onSubmit() {
    const p = {
      name: this.name,
      email: this.email,
      password: this.password,
    };
    let that = this;
    this.signupService.signup(p).subscribe(todoItem => {
      localStorage.setItem('login', 'true');
      that.router.navigateByUrl('/project');
    })
  }

}
