import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Router, Event, NavigationEnd } from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class AppHeaderComponent implements OnInit, OnDestroy {
  active: boolean = false;
  navigationSubscription;
  constructor(private authService: AuthService, private router: Router) {
    this.setHeaderActive();
  }
  setHeaderActive() {
    this.active = this.authService.isAuthenticated();
  }
  ngOnInit() {
    let that = this;
    this.navigationSubscription = this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        // Navigation Ended Successfully.
        this.setHeaderActive();
      }
    });
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

}
