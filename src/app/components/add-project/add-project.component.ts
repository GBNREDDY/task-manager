import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  @Output() addItem: EventEmitter<any> = new EventEmitter();
  @Input() type: string;
  name: string;
  constructor() { }

  ngOnInit() {
  }
  onSubmit() {
    this.addItem.emit(this.name);
    this.name = '';
  }
}
