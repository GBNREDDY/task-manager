import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  constructor(private router: Router, private loginService: LoginService) {
    if (localStorage.getItem('login') == 'true') {
      this.router.navigateByUrl('/project');
    }
  }

  ngOnInit() {
  }
  onSubmit() {
    const p = {
      email: this.email,
      password: this.password,
    };
    let that = this;
    this.loginService.login(p).subscribe(todoItem => {
      localStorage.setItem('login', 'true');
      that.router.navigateByUrl('/project');
    })
  }
}
