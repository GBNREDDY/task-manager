import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Todo } from '../models/Todo';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class TodoService {
  url: string = '';
  constructor(private http: HttpClient) { }

  getTodo(pid: string): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.url}/api/project/${pid}/todo`);
  }
  addTodo(project: Todo): Observable<Todo> {
    return this.http.post<Todo>(`${this.url}/api/todo`, project, httpOptions);
  }
  deleteTodo(id: number) {
    return this.http.delete<Todo>(`${this.url}/api/todo/${id}`, httpOptions);
  }
}
