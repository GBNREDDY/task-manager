import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Project } from '../models/project';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})

export class ProjectService {

  url: string = '';

  constructor(private http: HttpClient) { }

  getProject(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.url}/api/project`);
  }
  addProject(project: Project): Observable<Project> {
    return this.http.post<Project>(`${this.url}/api/project`, project, httpOptions);
  }
  deleteProject(id: number) {
    return this.http.delete<Project>(`${this.url}/api/project/${id}`, httpOptions);

  }
}
