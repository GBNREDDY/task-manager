import { TestBed } from '@angular/core/testing';

import { TodoItemDetailService } from './todo-item-detail.service';

describe('TodoItemDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TodoItemDetailService = TestBed.get(TodoItemDetailService);
    expect(service).toBeTruthy();
  });
});
