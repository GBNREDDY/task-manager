import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User } from '../models/user';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  url: string = '';

  constructor(private http: HttpClient) { }


  signup(user: User): Observable<User> {
    return this.http.post<User>(`${this.url}/api/signup`, user, httpOptions);
  }

}
