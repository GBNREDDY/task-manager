import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User } from '../models/user';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url: string = '';

  constructor(private http: HttpClient) { }


  login(user: User): Observable<User> {
    return this.http.post<User>(`${this.url}/api/login`, user, httpOptions);
  }

}
