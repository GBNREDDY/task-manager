import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { TodoItem } from '../models/todo-item';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class TodoItemService {
  url: string = '';

  constructor(private http: HttpClient) { }

  getTodoItem(pid: string, tid: string): Observable<TodoItem[]> {
    return this.http.get<TodoItem[]>(`${this.url}/api/project/${pid}/todo/${tid}/todo-item`);
  }
  addTodoItem(project: TodoItem): Observable<TodoItem> {
    return this.http.post<TodoItem>(`${this.url}/api/todo-item`, project, httpOptions);
  }
  updateTodoItem(project: TodoItem): Observable<TodoItem> {
    return this.http.put<TodoItem>(`${this.url}/api/todo-item`, project, httpOptions);
  }
  deleteTodoItem(id: number) {
    return this.http.delete<TodoItem>(`${this.url}/api/todo-item/${id}`, httpOptions);
  }

}
