import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuthenticated() {
    if (localStorage.getItem('login') == 'true') {
      return true;
    }
    return false;
  }

  constructor() { }
}
