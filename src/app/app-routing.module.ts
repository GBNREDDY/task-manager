import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectComponent } from './components/project/project.component';
import { TodoComponent } from './components/todo/todo.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { TodoItemDetailComponent } from './components/todo-item-detail/todo-item-detail.component'

import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'project', component: ProjectComponent, canActivate: [AuthGuard] },
  { path: 'project/:pid', component: TodoComponent, canActivate: [AuthGuard] },
  { path: 'project/:pid/todo/:tid', component: TodoItemComponent, canActivate: [AuthGuard] },
  { path: 'project/:pid/todo/:tid/item/:iid', component: TodoItemDetailComponent, canActivate: [AuthGuard] },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
