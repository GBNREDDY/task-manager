export class TodoItem {
    id?: number;
    name: string;
    project_id: number;
    todo_id: number;
    completed: boolean;
}