export class Todo {
    id?: number;
    name: string;
    project_id: number;
}