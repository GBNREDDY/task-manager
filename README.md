# Task Manager

A Todo list project

Angular CLI: 8.1.1                                                                                                         
Node: 12.6.0                                                                                                               
OS: win32 x64                                                                                                              
Angular: 8.1.1                                                                                                             
... animations, cli, common, compiler, compiler-cli, core, forms                                                          
... language-service, platform-browser, platform-browser-dynamic                                                           
... router                                                                                                                  
                                                                                                                          
Package                           Version                                                                                  
-----------------------------------------------------------                                                                
@angular-devkit/architect         0.801.1                                                                                  
@angular-devkit/build-angular     0.801.1                                                                                  
@angular-devkit/build-optimizer   0.801.1                                                                                  
@angular-devkit/build-webpack     0.801.1                                                                                  
@angular-devkit/core              8.1.1                                                                                    
@angular-devkit/schematics        8.1.1                                                                                    
@ngtools/webpack                  8.1.1                                                                                    
@schematics/angular               8.1.1                                                                                    
@schematics/update                0.801.1                                                                                  
rxjs                              6.4.0                                                                                    
typescript                        3.4.5                                                                                    
webpack                           4.35.2 